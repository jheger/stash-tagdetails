#!/bin/bash

#-----------------------------------------------------------------------------
# Create test data
#
# Usage:
#
# 1) Create a certificate for test user 'Tag User <tu@example.com>' calling 'gpg --gen-key'
#   On my gitbash, there where following problems. Here are the solutions.
#    o 1. gpg missed 'iconv.dll'. Download it, and copy it into the same directory as the 'gpg.exe' file is stored.
#    o 2. gpg missed directory <my win user home>/.gnupg. Create it.
# 2) Run this script
#-----------------------------------------------------------------------------

#set -x

#-----
# Globals
#-----
#g_url_context='stash'
g_url_context='bitbucket'
g_url_scheme='http'
g_bitbucket_url="localhost:7990/$g_url_context"
g_user='admin'
g_password='admin'


#-----
# Functions
#-----


# Convenience function for curl
function curl_post {
    local url="$1"
    local jsonData="$2"

    curl -H 'Content-type: application/json' -u 'admin:admin' --data "$jsonData" "$g_url_scheme://$g_bitbucket_url/$url"
}


# Create a new project.
function create_project {
    local key="$1"
    local name="$2"
    local description="$3"

    local data="{\"key\": \"$key\", \"name\": \"$name\", \"description\": \"$description\"}"
    curl_post 'rest/api/1.0/projects' "$data"
}


function delete_project {
    local projectKey="$1"

    curl -u 'admin:admin' -X DELETE "$g_url_scheme://$g_bitbucket_url/rest/api/1.0/projects/$projectKey" >/dev/null
}


function set_project_permission {
    local projectKey="$1"
    local permission="$2"

    curl_post "rest/api/1.0/projects/$projectKey/permissions/$permission/all?allow=true"
}


# Create a new repository.
function create_repository {
    local projectKey="$1"
    local name="$2"

    local data="{\"name\": \"$name\", \"scmId\": \"git\"}"
    curl_post "rest/api/1.0/projects/$projectKey/repos" "$data"
}


function delete_repository {
    local projectKey="$1"
    local repositorySlug="$2"

    curl -u 'admin:admin' -X DELETE "$g_url_scheme://$g_bitbucket_url/rest/api/1.0/projects/$projectKey/repos/$repositorySlug" >/dev/null
}


# git tag command hasn't an --author option. Set the tag author in repo's git config.
function setTagUser {
    git config user.name "Tag User"
    git config user.email tu@example.com
}


# git notes command hasn't an --author option. Set the tag author in repo's git config.
function setNotesUser {
    git config user.name "Notes User"
    git config user.email nu@example.com
}


#-----
# M A I N
#-----

project_key='IT'
project_name='Integrationtest'
repo_name='tagstestrepo'

delete_repository "$project_key" "$repo_name"
delete_project "$project_key"

create_project "$project_key" "$project_name" 'Description'
set_project_permission "$project_key" 'PROJECT_WRITE'
create_repository "$project_key" "$repo_name"

rm -rf "$repo_name"

git clone "http://admin:admin@lara:7990/$g_url_context/scm/$project_key/${repo_name}.git"
cd "$repo_name"


commit_dummy_description='This is a dummy description. This is a dummy description. This is a dummy description. This is a dummy description. This is a dummy description.'
lorem_ipsum='Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.'


#-----
# Branch master
#-----

echo "Line" > file_1.txt
git add .
git commit -q -m "Commit with annotated tag" -m "$commit_dummy_description" --author="Commit User <cu@example.com>"
setTagUser
git tag anno-1 -a -m "This is my first annotated tag"
setNotesUser
git notes add -m "This is a note"

echo "Line" >> file_1.txt
git add .
git commit -q -m "Second commit" -m "$commit_dummy_description" --author="Commit User <cu@example.com>"
git checkout -b b-branch
git checkout master

echo "Line" >> file_1.txt
git add .
git commit -q -m "Third commit. Has two annotated tags with long text which forces the dialog to show Y scroll bar." -m "$commit_dummy_description" \
    --author="Commit User <cu@example.com>"
setTagUser
git tag anno-2.1 -a -m "$lorem_ipsum"
git tag anno-2.2 -a -m "$lorem_ipsum" -m "$lorem_ipsum"

for i in {1..100}
do
    echo "Line" >> file_1.txt
    git add .
    git commit -q -m "Dummy commit $i to force paging the commits page" -m "$commit_dummy_description" --author="Commit User <cu@example.com>"
done


#-----
# Branch b1...
#-----


echo "Line" >> file_1.txt
git add .
git commit -q -m "Commit on branch b1 with single lightweight tag" -m "$commit_dummy_description" --author="Commit User <cu@example.com>"
setTagUser
git tag light-b1-1


#-----
# Branch master
#-----

git checkout master

echo "Line" >> file_1.txt
git add .
git commit -q -m "Commit with single lightweight tag" -m "$commit_dummy_description" --author="Commit User <cu@example.com>"
setTagUser
git tag light-1

echo "Line" >> file_1.txt
git add .
git commit -q -m "Commit with two lightweight tags" -m "$commit_dummy_description" --author="Commit User <cu@example.com>"
setTagUser
git tag light-2.1
git tag light-2.2

echo "Line" >> file_1.txt
git add .
git comit -q -m "Commit with single annotated tag and has a Jira issue EP-1" -m "$commit_dummy_description" --author="Commit User <cu@example.com>"
setTagUser
git tag anno-1.1 -a -m "This is an annotated tag"

echo "Line" >> file_1.txt
git add .
git commit -q -m "Commit with two annotated tags" -m "$commit_dummy_description" --author="Commit User <cu@example.com>"
setTagUser
git tag anno-3.1 -a -m "This is an annotated tag, set by 'Tag User'"
git config user.name "Tag User2"
git config user.email tu2@example.com
setTagUser
git tag anno-3.2 -a -m "This is an annotated tag, set by 'Tag User2'"
git config user.name "Tag User"
git config user.email tu@example.com

echo "Line" >> file_1.txt
git add .
git commit -q -m "Commit with annotated long messag'ed single line tag" -m "$commit_dummy_description" --author="Commit User <cu@example.com>"
setTagUser
git tag anno-sline-message -a -m \
"This is an annotated tag. Is has a long oneline message, which will lead Stash to flip the dialog box to the right of the tag in case leftside is not enough space."\
" In that case, the dialog box is not readable."

echo "Line" >> file_1.txt
git add .
git commit -q -m "Commit with annotated long messag'ed multi line tag" -m "$commit_dummy_description" --author="Commit User <cu@example.com>"
setTagUser
git tag anno-mline-message -a -m \
"This is an annotated tag. Is has a long multiline message, which will not lead
Stash to flip the dialog box to the right of the tag. (In that case, the dialog 
box would not be readable."

echo "Line" >> file_1.txt
git add .
git commit -q -m "Commit with annotated tag with special chars" -m "$commit_dummy_description" --author="Commit User <cu@example.com>"
setTagUser
git tag "a=&#/]@$'()+,;%!a" -a -m "a=&#/]@$'()+,;%!a"

# Needs gpg --gen-key
echo "Line" >> file_1.txt
git add .
git commit -q -m "Commit with signed tag" -m "$commit_dummy_description" --author="Commit User <cu@example.com>"
# Switch back to 'Tag User' because only he has a certificate. Pass phrase is 1234.
echo "**** Pass phrase is 1234 ****"
git config user.name "Tag User"
git config user.email tu@example.com
setTagUser
git tag anno-signed -s -m "This is a signed annotated tag"
setNotesUser
git notes add -m "This is a note"

echo "Line" >> file_1.txt
git add .
git commit -q -m "Commit with 10 annotated tags" -m "$commit_dummy_description" --author="Commit User <cu@example.com>"
setTagUser
git tag anno-10.1 -a -m "Annotated tag"
git tag anno-10.2 -a -m "Annotated tag"
git tag anno-10.3 -a -m "Annotated tag"
git tag anno-10.4 -a -m "Annotated tag"
git tag anno-10.5 -a -m "Annotated tag"
git tag anno-10.6 -a -m "Annotated tag"
git tag anno-10.7 -a -m "Annotated tag"
git tag anno-10.8 -a -m "Annotated tag"
git tag anno-10.9 -a -m "Annotated tag"
git tag anno-10.10 -a -m "Annotated tag"

echo "Line" >> file_1.txt
git add .
git commit -q -m "Commit with 9 annotated tags" -m "$commit_dummy_description" --author="Commit User <cu@example.com>"
setTagUser
git tag anno-9.1 -a -m "Annotated tag"
git tag anno-9.2 -a -m "Annotated tag"
git tag anno-9.3 -a -m "Annotated tag"
git tag anno-9.4 -a -m "Annotated tag"
git tag anno-9.5 -a -m "Annotated tag"
git tag anno-9.6 -a -m "Annotated tag"
git tag anno-9.7 -a -m "Annotated tag"
git tag anno-9.8 -a -m "Annotated tag"
git tag anno-9.9 -a -m "Annotated tag"

git checkout -b b1-for-merge
git checkout -b b2-for-octopus-merge
git checkout -b b3-for-octopus-merge

# Create merge commit. Tags on merge commits have one more attribute "Merge: abc def"
git checkout b1-for-merge
echo "Line" >> file_1.txt
git add .
git commit -q -m "Commit on b1-for-merge" -m "$commit_dummy_description" --author="Commit User <cu@example.com>"
git checkout master
echo "Line" >> file_1.txt
git add .
git commit -q -m "Commit on master for merge" -m "$commit_dummy_description" --author="Commit User <cu@example.com>"
git merge --no-edit --strategy=ours b1-for-merge
setTagUser
git tag anno-on-merge -a -m "This is an annotated tag on a merge commit"

# Create octopus merge commit. Tags on octopus merge commits have one more attribute "Merge: abc def 123"
git checkout b2-for-octopus-merge
echo "Line" >> file_1.txt
git add .
git commit -q -m "Commit on b2-for-octopus-merge" -m "$commit_dummy_description" --author="Commit User <cu@example.com>"
git checkout b3-for-octopus-merge
echo "Line" >> file_1.txt
git add .
git commit -q -m "Commit on b3-for-octopus-merge" -m "$commit_dummy_description" --author="Commit User <cu@example.com>"
git checkout master
echo "Line" >> file_1.txt
git add .
git commit -q -m "Commit on master for octupus merge" -m "$commit_dummy_description" --author="Commit User <cu@example.com>"
git merge --no-edit --strategy=ours b2-for-octopus-merge b3-for-octopus-merge
setTagUser
git tag anno-on-octopus-merge -a -m "This is an annotated tag on an octopus merge commit"
echo "Line" >> file_1.txt
git add .
git commit -q -m "EP-1 Commit with Jira issue" --author="Commit User <cu@example.com>"


# Show the result
echo ""
echo ""
#git log --decorate --oneline --graph


git push --all
git push --tags
git push origin refs/notes/*


read -r -d '' pullrequest_data <<'END'
{
    "title": "Pull Request",
    "description": "Pull Request",
    "state": "OPEN",
    "open": true,
    "closed": false,
    "fromRef": {
        "id": "refs/heads/master",
        "repository": {
            "slug": "tagstestrepo",
            "name": null,
            "project": {
                "key": "IT"
            }
        }
    },
    "toRef": {
        "id": "refs/heads/b-branch",
        "repository": {
            "slug": "tagstestrepo",
            "name": null,
            "project": {
                "key": "IT"
            }
        }
    }
}
END

curl_post "rest/api/1.0/projects/$project_key/repos/$repo_name/pull-requests" "$pullrequest_data"

#curl -u admin:admin -H "Content-Type: application/json" -X POST -d "$pullrequest_data" http://your_stash_url/rest/api/1.0/projects/$project_key/repos/$repo_name/pull-requests
