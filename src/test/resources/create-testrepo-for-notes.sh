#!/bin/bash


#set -x

#-----
# Globals
#-----
#g_url_context='stash'
g_url_context='bitbucket'
g_url_scheme='http'
g_bitbucket_url="localhost:7990/$g_url_context"
g_user='admin'
g_password='admin'


#-----
# Functions
#-----


# Convenience function for curl
function curl_post {
    local url="$1"
    local jsonData="$2"

    curl -H 'Content-type: application/json' -u 'admin:admin' --data "$jsonData" "$g_url_scheme://$g_bitbucket_url/$url"
}


# Create a new project.
function create_project {
    local key="$1"
    local name="$2"
    local description="$3"

    local data="{\"key\": \"$key\", \"name\": \"$name\", \"description\": \"$description\"}"
    curl_post 'rest/api/1.0/projects' "$data"
}


function delete_project {
    local projectKey="$1"

    curl -u 'admin:admin' -X DELETE "$g_url_scheme://$g_bitbucket_url/rest/api/1.0/projects/$projectKey" >/dev/null
}


function set_project_permission {
    local projectKey="$1"
    local permission="$2"

    curl_post "rest/api/1.0/projects/$projectKey/permissions/$permission/all?allow=true"
}


# Create a new repository.
function create_repository {
    local projectKey="$1"
    local name="$2"

    local data="{\"name\": \"$name\", \"scmId\": \"git\"}"
    curl_post "rest/api/1.0/projects/$projectKey/repos" "$data"
}


function delete_repository {
    local projectKey="$1"
    local repositorySlug="$2"

    curl -u 'admin:admin' -X DELETE "$g_url_scheme://$g_bitbucket_url/rest/api/1.0/projects/$projectKey/repos/$repositorySlug" >/dev/null
}

# git tag command hasn't an --author option. Set the tag author in repo's git config.
function setTagUser {
    git config user.name "Tag User"
    git config user.email tu@example.com
}


# git notes command hasn't an --author option. Set the tag author in repo's git config.
function setNotesUser {
    git config user.name "Notes User"
    git config user.email nu@example.com
}


# https://developer.atlassian.com/bitbucket/server/docs/latest/how-tos/updating-build-status-for-commits.html
function setBuildStatus {
    local commitId="$1"

read -r -d '' data <<'END'
{
    "state": "SUCCESSFUL",
    "key": "REPO-MASTER",
    "name": "REPO-MASTER-42",
    "url": "https://bamboo.example.com/browse/REPO-MASTER-42",
    "description": "Changes by John Doe"
}
END

    curl_post "rest/build-status/1.0/commits/$commitId" "$data"
}


#-----
# M A I N
#-----

project_key='IT'
repo_name='notestestrepo'

commit_dummy_description='This is a dummy description. This is a dummy description. This is a dummy description. This is a dummy description. This is a dummy description.'

delete_repository "$project_key" "$repo_name"
#delete_project "$project_key"

#create_project "$project_key" "$repo_name" 'Description'
#set_project_permission "$project_key" 'PROJECT_WRITE'
create_repository "$project_key" "$repo_name"

rm -rf "$repo_name"
git clone "http://admin:admin@lara:7990/$g_url_context/scm/$project_key/${repo_name}.git"
cd "$repo_name"

#commit_dummy_description='This is a dummy description. This is a dummy description. This is a dummy description. This is a dummy description. This is a dummy description.'
lorem_ipsum_oneline='Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.'

read -r -d '' lorem_ipsum_multiline <<'END'
  Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy
eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.


  At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren,
no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet,
consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et
dolore magna aliquyam erat, sed diam voluptua.

At vero eos et accusam et justo duo dolores et ea rebum.

Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.
END



# Branch master
echo "Line" > file_1.txt
git add .
git commit -q -m "Commit" --author="Commit User <cu@example.com>"

git checkout -b branch-b1
git checkout -b branch-b2
git checkout -b branch-b3
git checkout master
echo "Line" >> file_1.txt
git add .
git commit -q -m "Commit on master branch-b1 branch-b2 branch-b3" --author="Commit User <cu@example.com>"

echo "Line" >> file_1.txt
git add .
git commit -q -m "Commit with note in default namespace" -m "$commit_dummy_description" --author="Commit User <cu@example.com>"
setNotesUser
git notes add -m "This is note-10.1"

git checkout -b branch-b4
git checkout master
echo "Line" >> file_1.txt
git add .
git commit -q -m "Commit on master branch-b4" --author="Commit User <cu@example.com>"

for i in {1..100}
do
    echo "Line" >> file_1.txt
    git add .
    git commit -q --allow-empty -m "Dummy commit $i to force paging the commits page" -m "$commit_dummy_description" --author="Commit User <cu@example.com>"

    if [ $i = 50 ]
    then
        echo "********** setBuildStatus"
        setBuildStatus `git show --format='%H' -s`
    fi
done

echo "Line" >> file_1.txt
git add .
git commit -q -m "Commit without notes" --author="Commit User <cu@example.com>"
setTagUser
git tag no-notes

echo "Line" >> file_1.txt
git add .
git commit -q -m "Commit with note in default namespace, notes contains special chars" -m "$commit_dummy_description" --author="Commit User <cu@example.com>"
setNotesUser
git notes add -m "This is note-10.0 with special chars: 'Ä', <b>bold</> & \" "

echo "Line" >> file_1.txt
git add .
git commit -q -m "Commit with multi line note in default namespace" -m "$commit_dummy_description" --author="Commit User <cu@example.com>"
setNotesUser
git notes add -m "This is note-15.0" -m "And this is the next line" -m "And another line"

echo "Line" >> file_1.txt
git add .
git commit -q -m "Commit with note-20.1 in namespace_1" -m "$commit_dummy_description" --author="Commit User <cu@example.com>"
setNotesUser
git notes --ref=namespace_1 add -m "This is note-20.1 in namespace_1"

echo "Line" >> file_1.txt
git add .
git commit -q -m "Commit with note-25.0 in default namespace and note-25.2 in namespace_2" -m "$commit_dummy_description" --author="Commit User <cu@example.com>"
setNotesUser
git notes add -m "This is note-25.0"
git notes --ref=namespace_2 add -m "This is note-25.2 in namespace_2"

echo "Line" >> file_1.txt
git add .
git commit -q -m "Commit with note-30.0 in default namespace, note-30.1 in namespace_1 and note-30.2 in namespace_2" -m "$commit_dummy_description" --author="Commit User <cu@example.com>"
setNotesUser
git notes add -m "This is a note-30.0"
git notes --ref=namespace_1 add -m "This is note-30.1 in namespace_1"
git notes --ref=namespace_2 add -m "This is note-30.2 in namespace_2"
setTagUser
git tag anno-1 -a -m "This is an annotated tag"

echo "Line" >> file_1.txt
git add .
git commit -q -m "Commit with lorem ipsum online in default namespace, namespace_1 and namespace_2" -m "$commit_dummy_description" --author="Commit User <cu@example.com>"
setNotesUser
git notes add -m "$lorem_ipsum_oneline"
git notes --ref=namespace_1 add -m "$lorem_ipsum_oneline"
git notes --ref=namespace_2 add -m "$lorem_ipsum_oneline"

echo "Line" >> file_1.txt
git add .
git commit -q -m "Commit with lorem ipsum multiline in default namespace, namespace_1 and namespace_2" -m "$commit_dummy_description" --author="Commit User <cu@example.com>"
setNotesUser
git notes add -m "$lorem_ipsum_multiline"
git notes --ref=namespace_1 add -m "$lorem_ipsum_multiline"
git notes --ref=namespace_2 add -m "$lorem_ipsum_multiline"


git push --all
git push --tags
git push origin refs/notes/*

read -r -d '' pullrequest_data <<'END'
{
    "title": "Pull Request with Notes on first commits page",
    "description": "Pull Request with Notes on first commits page",
    "state": "OPEN",
    "open": true,
    "closed": false,
    "fromRef": {
        "id": "refs/heads/master",
        "repository": {
            "slug": "notestestrepo",
            "name": null,
            "project": {
                "key": "IT"
            }
        }
    },
    "toRef": {
        "id": "refs/heads/branch-b1",
        "repository": {
            "slug": "notestestrepo",
            "name": null,
            "project": {
                "key": "IT"
            }
        }
    }
}
END
curl_post "rest/api/1.0/projects/$project_key/repos/$repo_name/pull-requests" "$pullrequest_data"


read -r -d '' pullrequest_data <<'END'
{
    "title": "Pull Request with Notes on subsequent commits page",
    "description": "Pull Request with Notes on subsequent commits page",
    "state": "OPEN",
    "open": true,
    "closed": false,
    "fromRef": {
        "id": "refs/tags/no-notes",
        "repository": {
            "slug": "notestestrepo",
            "name": null,
            "project": {
                "key": "IT"
            }
        }
    },
    "toRef": {
        "id": "refs/heads/branch-b2",
        "repository": {
            "slug": "notestestrepo",
            "name": null,
            "project": {
                "key": "IT"
            }
        }
    }
}
END
curl_post "rest/api/1.0/projects/$project_key/repos/$repo_name/pull-requests" "$pullrequest_data"