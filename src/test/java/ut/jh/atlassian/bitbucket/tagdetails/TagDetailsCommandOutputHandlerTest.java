package ut.jh.atlassian.bitbucket.tagdetails;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.endsWith;
import static org.hamcrest.Matchers.startsWith;

import java.io.InputStream;
import java.nio.charset.UnsupportedCharsetException;

import jh.atlassian.bitbucket.tagdetails.TagDetails;
import jh.atlassian.bitbucket.tagdetails.TagDetailsCommandOutputHandler;

import org.apache.commons.io.IOUtils;
import org.junit.Test;


// All outputs with command: git show <tag name> -q
public class TagDetailsCommandOutputHandlerTest {
    TagDetailsCommandOutputHandler parser = new TagDetailsCommandOutputHandler();

    private InputStream stringToStream(String tagInfoString) {
        InputStream in = null;
        try {
            in = IOUtils.toInputStream(tagInfoString, "UTF-8");
        } catch (UnsupportedCharsetException e) {
            e.printStackTrace();
        }
        return in;
    }

    // @Test
    public void testLightweightTag() {
        String s = "commit 2ce6ca43f4136e51c16a285c261d478fbaeaab16\n"
                + "Author: Commit User <cu@example.com>\n"
                + "Date:   Sun May 3 17:33:15 2015 +0200\n"
                + "\n"
                + "    Commit with single leightweight tag\n";

        parser.process(stringToStream(s));
        TagDetails tagDetails = parser.getOutput();
        assertThat(tagDetails, is(not(nullValue())));
        assertThat(tagDetails.getTagname(), is("2ce6ca43f4136e51c16a285c261d478fbaeaab16"));
        assertThat(tagDetails.getTagger(), is(nullValue()));
        assertThat(tagDetails.getDate(), is(nullValue()));
        assertThat(tagDetails.getMessage(), is(nullValue()));
        assertThat(tagDetails.getCommit(), is(nullValue()));
    }

    @Test
    public void testAnnotatedTagOfMergeCommit() {
        String s = "tag anno-1\n"
                + "Tagger: Tag User <tu@example.com>\n"
                + "Date:   Sun May 3 17:33:15 2015 +0200\n"
                + "\n"
                + "This is an annotated tag\n"
                + "\n"
                + "commit 733cb0bc4f8a0419688ac13e9c431e497c07da9a\n"
                + "Author: Commit User <cu@example.com>\n"
                + "Date:   Sun May 3 17:33:15 2015 +0200\n"
                + "\n"
                + "    Commit with single annotated tag\n";

        parser.process(stringToStream(s));
        TagDetails tagDetails = parser.getOutput();
        assertThat(tagDetails, is(not(nullValue())));
        assertThat(tagDetails.getTagname(), is("anno-1"));
        assertThat(tagDetails.getTagger(), is("Tag User <tu@example.com>"));
        assertThat(tagDetails.getDate(), is("Sun May 3 17:33:15 2015 +0200"));
        assertThat(tagDetails.getMessage(), is("This is an annotated tag"));
        assertThat(tagDetails.getCommit(), is("733cb0bc4f8a0419688ac13e9c431e497c07da9a"));
    }

    @Test
    public void testAnnotatedTag_() {
        String s = "tag merge-b1-into-master\n"
                + "Tagger: Tag User <tu@example.com>\n"
                + "Date:   Fri Jul 24 10:51:46 2015 +0200\n\n"
                + "Description of Tag merge-b1-into-master\n\n"
                + "commit fa04a983c8e64e4bd62a061d109ddf35291b71e7\n"
                + "Merge: 45c951c 107faed\n"
                + "Author: Tag User <tu@example.com>\n"
                + "Date:   Thu Jul 16 11:35:42 2015 +0200\n\n"
                + "    Merge pull request #1 in PROJECT_1/it from b1 to master\n\n"
                + "* commit '8007dcd259471d85fb7f89da4346e8cdbfaf3654':\n"
                + "  Commit on branch b1 with single lightweight tag\n\n";

        parser.process(stringToStream(s));
        TagDetails tagDetails = parser.getOutput();
        assertThat(tagDetails, is(not(nullValue())));
        assertThat(tagDetails.getTagname(), is("merge-b1-into-master"));
        assertThat(tagDetails.getTagger(), is("Tag User <tu@example.com>"));
        assertThat(tagDetails.getDate(), is("Fri Jul 24 10:51:46 2015 +0200"));
        assertThat(tagDetails.getMessage(), is("Description of Tag merge-b1-into-master"));
        assertThat(tagDetails.getCommit(), is("fa04a983c8e64e4bd62a061d109ddf35291b71e7"));
    }

    @Test
    public void testAnnotatedTagWithMessageInLongSingleLine() {
        String s = "tag anno-sline-message\n"
                + "Tagger: Tag User2 <tu2@example.com>\n"
                + "Date:   Sun May 3 17:33:16 2015 +0200\n"
                + "\n"
                + "This is an annotated tag. Is has a long oneline message, which will lead Stash to flip the dialog box to the right of the tag in case leftside is not enough space. In that case, the dialog box is not readable.\n"
                + "\n"
                + "commit 7e0bc9dd4f54502eb30f07d9628157b5036ff15d\n"
                + "Author: Commit User <cu@example.com>\n"
                + "Date:   Sun May 3 17:33:16 2015 +0200\n"
                + "\n"
                + "    Commit with annotated long messag'ed single line tag\n";

        parser.process(stringToStream(s));
        TagDetails tagDetails = parser.getOutput();
        assertThat(tagDetails, is(not(nullValue())));
        assertThat(tagDetails.getTagname(), is("anno-sline-message"));
        assertThat(tagDetails.getTagger(), is("Tag User2 <tu2@example.com>"));
        assertThat(tagDetails.getDate(), is("Sun May 3 17:33:16 2015 +0200"));
        assertThat(
                tagDetails.getMessage(),
                is("This is an annotated tag. Is has a long oneline message, which will lead Stash to flip the dialog box to the right of the tag in case leftside is not enough space. In that case, the dialog box is not readable."));
        assertThat(tagDetails.getCommit(), is("7e0bc9dd4f54502eb30f07d9628157b5036ff15d"));
    }

    @Test
    public void testAnnotatedTagWithMessageInMultipleLines() {
        String s = "tag anno-mline-message\n"
                + "Tagger: Tag User2 <tu2@example.com>\n"
                + "Date:   Sun May 3 17:33:16 2015 +0200\n"
                + "\n"
                + "This is an annotated tag. Is has a long multiline message, which will not lead\n"
                + "Stash to flip the dialog box to the right of the tag. (In that case, the dialog\n"
                + "box would not be readable.\n"
                + "\n"
                + "commit b583c24c613daa186e61857dab0fce9b009f9d5c\n"
                + "Author: Commit User <cu@example.com>\n"
                + "Date:   Sun May 3 17:33:16 2015 +0200\n"
                + "\n"
                + "    Commit with annotated long messag'ed multi line tag\n";

        parser.process(stringToStream(s));
        TagDetails tagDetails = parser.getOutput();
        assertThat(tagDetails, is(not(nullValue())));
        assertThat(tagDetails.getTagname(), is("anno-mline-message"));
        assertThat(tagDetails.getTagger(), is("Tag User2 <tu2@example.com>"));
        assertThat(tagDetails.getDate(), is("Sun May 3 17:33:16 2015 +0200"));
        assertThat(tagDetails.getMessage(), is("This is an annotated tag. Is has a long multiline message, which will not lead\n"
                + "Stash to flip the dialog box to the right of the tag. (In that case, the dialog\n"
                + "box would not be readable."));
        assertThat(tagDetails.getCommit(), is("b583c24c613daa186e61857dab0fce9b009f9d5c"));
    }

    // Note the different PGP section in contrast to next test
    @Test
    public void testSignedTagWithPgPComment() {
        String s = "tag signed_tag\n"
                + "Tagger: Bryan Turner <bturner@atlassian.com>\n"
                + "Date:   Thu Dec 29 13:23:16 2011 +1100\n"
                + "\n"
                + "Creating a signed tag for testing\n"
                + "-----BEGIN PGP SIGNATURE-----\n"
                + "Version: GnuPG/MacGPG2 v2.0.17 (Darwin)\n"
                + "Comment: GPGTools - http://gpgtools.org\n" // diff
                + "\n"
                + "iQEcBAABAgAGBQJO+88jAAoJEH0nY8IfN+5vawsIAKS+WjVFN5/ux1XBmY1jIdan\n"
                + "diJy2FvdLdKf5iwpFidF0wDJ0JNGR5b4r5R7rrDLjL95wp2o8lEjTZuL0bQsSb41\n"
                + "965ZBSTgjuD49I95dX8TTDXTF5BeTTTK7Nt8UoXuiJRkSJqrZpA5vCI56/XbC8zN\n"
                + "klIjLJ4KYb9+cvvMYLubCaqRbo3zpkXWQBkStRAyJa23lSYXOlAp921g9ww1BCRF\n"
                + "+WUvDlUgzjmMra+uuhdOyxhFkZTia2YXdQR+Ip3+LqAAdrCuNKTk+/wZ8edIba2N\n"
                + "DvRrgZU4cijCxBZwFusZWAZQdHQ4Qjcxydr+Q2v5j/g204g7+zdddfcY6KEW7TQ=\n"
                + "=+Xrk\n"
                + "-----END PGP SIGNATURE-----\n"
                + "\n"
                + "commit 0a943a29376f2336b78312d99e65da17048951db\n"
                + "Author: Anna Buttfield <abuttfield@cascade.sydney.atlassian.com>\n"
                + "Date:   Thu Feb 3 16:01:07 2011 +1100\n"
                + "\n"
                + "    Copy C.zip as D.zip\n";

        parser.process(stringToStream(s));
        TagDetails tagDetails = parser.getOutput();
        assertThat(tagDetails, is(not(nullValue())));
        assertThat(tagDetails.getTagname(), is("signed_tag"));
        assertThat(tagDetails.getTagger(), is("Bryan Turner <bturner@atlassian.com>"));
        assertThat(tagDetails.getDate(), is("Thu Dec 29 13:23:16 2011 +1100"));
        assertThat(tagDetails.getMessage(), is("Creating a signed tag for testing"));
        assertThat(tagDetails.getCommit(), is("0a943a29376f2336b78312d99e65da17048951db"));
    }

    @Test
    public void testSignedTagWithoutPgPComment() {
        String s = "tag anno-signed\n"
                + "Tagger: Tag User <tu@example.com>\n"
                + "Date:   Sun May 3 17:33:16 2015 +0200\n"
                + "\n"
                + "This is a signed annotated tag\n"
                + "-----BEGIN PGP SIGNATURE-----\n"
                + "Version: GnuPG v1.4.13 (MingW32)\n"
                + "\n"
                + "iQEcBAABAgAGBQJVRj+8AAoJEJKiSy0U7Ak80bwH+wZeQX4LB/K/ZXBzjIR3Qvr7\n"
                + "MMp86Zg1SRrFAg67J0Ko0S47uqte6OnuYpLzqQ5W0/9y9UCRmivRdtNzENJRdP3/\n"
                + "NBF48NtOLc2dKAmLi3caIq8nldig2W9K62KN2ORnch74NUFHo6sdLUaemLFOwAcL\n"
                + "O94MeDS/paHq4TMw8GcthnVZTkqgamMnOphfO3ZgLSjqZzy+A4Ggn3VnXUEd/wXx\n"
                + "lhjP3vG1lJ79uLT4nQmA/bcL/nG623DImVCboilhakRz0eZFDNlY4fi4wFaoQZKH\n"
                + "atYdKJRT0Bn969oBED//t/AjTjr3LmJbCZKZVulH/ZgsUYY9Ig3Cx+ChRAEbmc4=\n"
                + "=euCC\n"
                + "-----END PGP SIGNATURE-----\n"
                + "\n"
                + "commit 87c72f01cb591c990c9cf54d1c578e3a3eb39419\n"
                + "Author: Commit User <cu@example.com>\n"
                + "Date:   Sun May 3 17:33:16 2015 +0200\n"
                + "\n"
                + "    Commit with signed tag\n"
                + "";

        parser.process(stringToStream(s));
        TagDetails tagDetails = parser.getOutput();
        assertThat(tagDetails, is(not(nullValue())));
        assertThat(tagDetails.getTagname(), is("anno-signed"));
        assertThat(tagDetails.getTagger(), is("Tag User <tu@example.com>"));
        assertThat(tagDetails.getDate(), is("Sun May 3 17:33:16 2015 +0200"));
        assertThat(tagDetails.getMessage(), is("This is a signed annotated tag"));
        assertThat(tagDetails.getCommit(), is("87c72f01cb591c990c9cf54d1c578e3a3eb39419"));
    }

    @Test
    public void test1() {
        String s = "tag lorem-ipsum\n"
                + "Tagger: Tag User <tu@example.de>\n"
                + "Date:   Wed Apr 29 21:43:48 2015 +0200\n"
                + "\n"
                + "Shows one or more objects (blobs, trees, tags and commits).\n"
                + "\n"
                + "For commits it shows the log message and textual diff. It also presents the merge commit in a special format as produced by git diff-tree --cc.\n"
                + "\n"
                + "For tags, it shows the tag message and the referenced objects.\n"
                + "\n"
                + "For trees, it shows the names (equivalent to git ls-tree with \\--name-only).\n"
                + "\n"
                + "For plain blobs, it shows the plain contents.\n"
                + "\n"
                + "The command takes options applicable to the git diff-tree command to control how the changes the commit introduces are shown.\n"
                + "\n"
                + "This manual page describes only the most frequently used options.\n"
                + "\n"
                + "commit 884db82243c941b1e22493f032b09cd574453563\n"
                + "Author: Heger, Johannes <Johannes.Heger@comdirect.de>\n"
                + "Date:   Wed Apr 29 21:39:24 2015 +0200\n"
                + "\n"
                + "    Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy\n"
                + "    eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam\n"
                + "    voluptua. At vero eos et accusam et justo duo dolores et ea rebum.\n"
                + "\n"
                + "    Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor\n"
                + "    sit amet.\n"
                + "\n"
                + "    Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy\n"
                + "    eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam\n"
                + "    voluptua. At vero eos et accusam et justo duo dolores et ea rebum.\n"
                + "\n"
                + "    Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor\n"
                + "    sit amet.\n";

        parser.process(stringToStream(s));
        TagDetails tagDetails = parser.getOutput();
        assertThat(tagDetails, is(not(nullValue())));
        assertThat(tagDetails.getTagname(), is("lorem-ipsum"));
        assertThat(tagDetails.getTagger(), is("Tag User <tu@example.de>"));
        assertThat(tagDetails.getDate(), is("Wed Apr 29 21:43:48 2015 +0200"));
        assertThat(tagDetails.getMessage(), startsWith("Shows one or more objects"));
        assertThat(tagDetails.getMessage(), endsWith("most frequently used options."));
        assertThat(tagDetails.getCommit(), is("884db82243c941b1e22493f032b09cd574453563"));
    }

}
