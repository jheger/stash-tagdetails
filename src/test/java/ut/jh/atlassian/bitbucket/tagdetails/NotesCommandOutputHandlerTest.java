package ut.jh.atlassian.bitbucket.tagdetails;

import jh.atlassian.bitbucket.tagdetails.NotesCommandOutputHandler;
import jh.atlassian.bitbucket.tagdetails.NotesModel;
import org.apache.commons.io.IOUtils;
import org.junit.Test;

import java.io.InputStream;
import java.nio.charset.UnsupportedCharsetException;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;


public class NotesCommandOutputHandlerTest {
    NotesCommandOutputHandler parser = new NotesCommandOutputHandler();

    private InputStream stringToStream(String tagInfoString) {
        InputStream in = null;
        try {
            in = IOUtils.toInputStream(tagInfoString, "UTF-8");
        } catch (UnsupportedCharsetException e) {
            e.printStackTrace();
        }
        return in;
    }

    @Test
    public void testNoNotes() {
        String s = "commit a026a8cb4457aa38225299b1ea9434b6b0684d3f\n" +
                "Author: Commit User <cu@example.com>\n" +
                "Date:   Tue Jan 24 00:00:00 2017 +0100\n" +
                "\n" +
                "    Commit without notes\n";

        parser.process(stringToStream(s));
        List<NotesModel> notesModels = parser.getOutput();
        assertThat(notesModels.size(), is(0));
    }

    @Test
    public void testNotesInDefaultNamespace() {
        String s = "commit a026a8cb4457aa38225299b1ea9434b6b0684d3f\n" +
                "Author: Commit User <cu@example.com>\n" +
                "Date:   Tue Jan 24 00:00:00 2017 +0100\n" +
                "\n" +
                "    Commit with notes in default namespace\n" +
                "\n" +
                "Notes:\n" +
                "    Note in default namespace\n" +
                "\n";

        parser.process(stringToStream(s));
        List<NotesModel> notesModels = parser.getOutput();
        assertThat(notesModels.size(), is(1));
        assertThat(notesModels.get(0).getNamespaceToNotesMap().get("Notes:"), is("Note in default namespace"));
    }

    @Test
    public void testNotesInDefaultNamespaceWithMultilineMessageWithTrailingNewlines() {
        String s = "commit a026a8cb4457aa38225299b1ea9434b6b0684d3f\n" +
                "Author: Commit User <cu@example.com>\n" +
                "Date:   Tue Jan 24 00:00:00 2017 +0100\n" +
                "\n" +
                "    Commit with notes in default namespace\n" +
                "\n" +
                "Notes:\n" +
                "    Note in default namespace\n" +
                "\n"
                + "      Indented by 2 spaces, with trailing newlines\n\n";

        parser.process(stringToStream(s));
        List<NotesModel> notesModels = parser.getOutput();
        assertThat(notesModels.size(), is(1));
        assertThat(notesModels.get(0).getNamespaceToNotesMap().get("Notes:"), is("Note in default namespace\n\n  Indented by 2 spaces, with trailing newlines"));
    }

    @Test
    public void testNotesInDefaultNamespaceWithMultilineMessage() {
        String s = "commit d9ee8838aec516984171fee551de4e2de6dcae73\n" +
                "Author: Commit User <cu@example.com>\n" +
                "Date:   Mon Apr 24 21:53:51 2017 +0200\n" +
                "\n" +
                "    Commit with note default namespace, namespace_1 and namespace_2\n" +
                "\n" +
                "Notes:\n" +
                "      Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy\n" +
                "    eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.\n" +
                "\n" +
                "\n" +
                "      At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren,\n" +
                "    no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet,\n" +
                "    consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et\n" +
                "    dolore magna aliquyam erat, sed diam voluptua.\n" +
                "\n" +
                "    At vero eos et accusam et justo duo dolores et ea rebum.\n" +
                "\n" +
                "    Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.";

        String expectedNotesMessage = "  Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy\n" +
                "eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.\n" +
                "\n" +
                "\n" +
                "  At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren,\n" +
                "no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet,\n" +
                "consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et\n" +
                "dolore magna aliquyam erat, sed diam voluptua.\n" +
                "\n" +
                "At vero eos et accusam et justo duo dolores et ea rebum.\n" +
                "\n" +
                "Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.";

        parser.process(stringToStream(s));
        List<NotesModel> notesModels = parser.getOutput();
        assertThat(notesModels.size(), is(1));
        assertThat(notesModels.get(0).getNamespaceToNotesMap().get("Notes:"), is(expectedNotesMessage));
    }

}
