import {ButtonExtension} from '@atlassian/clientside-extensions';

/**
 * @clientside-extension
 * @extension-point bitbucket.ui.pullrequest.commits.table.column.after
 */
export default ButtonExtension.factory((pluginApi, context) => {

    //console.log(' ButtonExtension bitbucket.ui.pullrequest.commits.table.column.after');

    let isHidden = true;
    // The label is required as string. The value '0' should never be displayed, as this is a placeholder meaning
    // "not yet evaluated if any notes present for this commit", and as long as this is not evaluated the button
    // shall be disabled.
    let notesCount = '0';

    const commitId = context.commit.id;
    //console.log('  commitId ' + commitId);

    // Check if there are notes-data waiting for a notes-button.
    // If yes, direct the notes-data to the button.
    // If not, put the button into the queue of waiting buttons to be graped from notes-data later on when notes-data
    // arrives.
    const waitingNotesData = bitbucketTagdetailsCache.getNotesData(commitId);
    if (waitingNotesData) {
        isHidden = false;
        // The label is required as string.
        notesCount = Object.keys(waitingNotesData.namespaceToNotesMap).length + '';
    } else {
        // There was no waiting notes-data for this notes-button. Put the notes-button into the queue of notes-buttons.
        bitbucketTagdetailsCache.addButtonAPI(commitId, pluginApi);
    }

    return {
        hidden: isHidden,
        header: 'Notes',
        iconBefore: 'page',
        label: notesCount,
        className: 'bitbucket-tagdetails-notesColumn_cse',
        onAction: () => {
        },
    };
});
