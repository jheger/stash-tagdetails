define('jh/atlassian/bitbucket/tagdetails', ['jquery', '@atlassian/aui', 'lodash', 'bitbucket/util/state'], function ($, AJS, _, state) {

    const IS_LOGGING = false;
    const DIALOG_HIDE_DELAY_MS = 100;
    let gHaveNotesSeen = false;
    // At "infinite scroll", continue numbering the tags- and notes-dialogs.
    let gDialogIndex = 0;

    const COMMITS_PAGE_RE = new RegExp('\/rest\/tags\/.+?\/tags$');
    const COMMIT_VIEW_PAGE_RE = new RegExp('\/rest\/branch-utils\/');
    const CREATE_TAG_ON_SINGLE_COMMIT_PAGE_RE = new RegExp('\/rest\/git\/.+?\/tags$');
    const DELETE_TAG_ON_SINGLE_COMMIT_PAGE_RE = new RegExp('\/rest\/git\/.+?\/tags/.+$');
    const PULLREQUEST_VIEW_PAGE_COMMITS_TAB_RE = new RegExp('\/pull-requests\/[0-9]+\/commits');
    const PULLREQUEST_CREATE_PAGE_COMMITS_TAB_RE = new RegExp('\/projects\.+?\/commits\\?');

    const URL_REGEX_ARRAY = [
        COMMITS_PAGE_RE,
        COMMIT_VIEW_PAGE_RE,
        CREATE_TAG_ON_SINGLE_COMMIT_PAGE_RE,
        DELETE_TAG_ON_SINGLE_COMMIT_PAGE_RE,
        PULLREQUEST_VIEW_PAGE_COMMITS_TAB_RE,
        PULLREQUEST_CREATE_PAGE_COMMITS_TAB_RE,
    ];


    // Use this regex to remove the time stamp from logfile:
    // ^\d\d\d\d-\d\d-\d\d\s\d\d:\d\d:\d\d,\d\d\d
    function log(message) {
        if (IS_LOGGING) {
            AJS.log(moment().format('YYYY-MM-DD HH:mm:ss,SSS') + ' Tag Details ' + message);
        }
    }

    function buildTagdetailsRestBaseUrl() {
        return AJS.contextPath() + '/rest/tagdetails/1.0/projects/' + state.getProject().key + '/repos/' + state.getRepository().slug;
    }

    //
    //  Handles the Notes column:
    //  This column is hidden by default. But if there is any note, the column shall be shown.
    //  This observer lets the Notes column be displayed after an infinite scroll update, even there isn't any more note on the new page.
    //
    function registerCommitsTableMutationObserver() {
        if (window.MutationObserver) {
            log('registerCommitsTableMutationObserver()');
            // Create a static commitsTableMutationObserver instance. This can be registered multiple times but gets called only once.
            const commitsTableMutationObserver = new MutationObserver(function (mutations) {
                mutations.forEach(function (mutation) {
                    const $mutationTarget = $(mutation.target);
                    log('CommitsTableMutationObserver: mutation type: ' + mutation.type
                        + ', target ' + mutation.target
                        //+ ', addedNodes ' + JSON.stringify(mutation.addedNodes)
                        //+ ', removedNodes ' + JSON.stringify(mutation.removedNodes)
                        + ', attributeName: ' + mutation.attributeName
                        + ', value: ' + $mutationTarget.attr(mutation.attributeName)
                        + ', oldValue: ' + mutation.oldValue);

                    if (mutation.type === 'attributes') {    // Should always 'attributes' because of the config. But for sure.
                        if (mutation.attributeName === 'class' && $mutationTarget.hasClass('no-rows')) {
                            // User changed source or destination branch on Create Pull-Request page. Because we're still on the same page,
                            // clear gHaveNotesSeen.
                            log('  On commits-table pruned. Setting gHaveNotesSeen to false.');
                            // After migration from former JS InlineDialog to <aui-inline-dialog>, the dialog accidentally could be kept open
                            // and positioned to the top left screen position.
                            // To avoid this, remove all aui-inline-dialogs when the commits-table content changes.
                            $('.tagdetails-aui-inline-dialog').remove();
                            gHaveNotesSeen = false;
                            $('.bitbucket-tagdetails-notesColumn').hide();
                        } else if (mutation.attributeName === 'data-last-updated') {
                            // As per CSS, the notes-column is hidden by default. This is OK on the first page of a commits-list. But on
                            // subsequent pages not:
                            // Imagine there are two pages of commits. On the first page there are notes, and on the second page aren't.
                            // As per CSS, the Notes-column is hidden. But this JS makes is visible.
                            // You scroll to the second page. The notes column will be hidden because of the CSS. And this JS will not make
                            // is visible. We have to make sure that the column and the heading are shown if there is at least one note
                            // already shown, AND it must be kept visible. This is done by observing the commits-table and show() the notes
                            // column after any insertion of new commits, if they have notes. The commits-table has the attribute
                            // 'data-last-updated' which changes is value at insertion.
                            log('  On infinite scroll, gHaveNotesSeen ' + gHaveNotesSeen);
                            if (gHaveNotesSeen) {
                                // Notes-column is 'display: none' by default by css. Show it in case there have been at least one note.
                                $('.bitbucket-tagdetails-notesColumn').show();
                            }
                        }
                    }
                });
            });

            // Observe only on commits-table. On single commit page .observe() would result in error:
            // "Failed to execute 'observe' on 'MutationObserver': parameter 1 is not of type 'Node'."
            const $commitsTable = $('.commits-table');
            if ($commitsTable.length > 0) {
                log('  commitsTableMutationObserver.observe()');
                // This commitsTableMutationObserver reacts multiple times on same value for data-last-updated. Don't know why.
                // attribute: data-last-updated
                const config = {
                    //childList: true,
                    attributes: true,
                    // "Set to true to record the previous value of any attribute that changes when monitoring the node or nodes for
                    // attribute changes." Only for logging.
                    attributeOldValue: true,
                    //subtree: true
                };
                commitsTableMutationObserver.observe($commitsTable.get(0), config);
            }
        }
    }

    function registerCSENotesButtonsObserver() {

        function dbgNodeListToString(nodeList) {
            let s = 'size ' + nodeList.length + ', nodes: ';
            const nodes = [];
            for (const node of nodeList.values()) {
                const isNotesTD = $(node).find('.bitbucket-tagdetails-notesColumn_cse').length;
                nodes.push(node.nodeName + ', isNotesTD ' + isNotesTD + ', text ' + $(node).text());
            }
            s += '[' + nodes.join(',') + ']';
            return s;
        }

        /*
        * Return the commit-ID given in den Commit-column in case the table-row has Notes.
        *
        * Unfortunately Bitbucket renders the CSE-column for Notes differently dependent on if the row is on the first
        * page or a subsequent page:
        *   - On the first page the mutation contains the TD including the DIV with the Notes-HTML.
        *   - On subsequent pages a first mutation contains the TD, and a second one contains the DIV with the
        *       Notes-HTML.
        *
        * @param mutation A mutation.
        * @returns The commit-ID in case the mutation is a Notes-column. Otherwise undefined.
        */
        function getCommitIdIfNotesTD(mutation) {

            if (mutation.type === 'childList' && mutation.addedNodes.length === 1) {
                const node = mutation.addedNodes[0];
                if (node.nodeName === 'TH' && node.textContent === 'Notes') {
                    return 'TH';
                }

                //log('getCommitIdIfNotesTD(), type ' + mutation.type + ', length ' + mutation.addedNodes.length);
                // The TD or the DIV comes as single mutations (addedNodes.length = 1).
                if (node.nodeName === 'TD' || node.nodeName === 'DIV') {
                    //log('  outerHTML ' + mutation.addedNodes[0].outerHTML);
                    //log('  TD index ' + $(mutation.addedNodes[0]).closest('TD').index());
                    const $node = $(mutation.addedNodes[0]);
                    const containsOrIsNotesDIV =
                        // Look for the class on child-nodes.
                        $node.find('.bitbucket-tagdetails-notesColumn_cse').length > 0 ||
                        // Look for the class on itself.
                        $node.filter('.bitbucket-tagdetails-notesColumn_cse').length > 0;
                    //log('  containsOrIsNotesDIV ' + containsOrIsNotesDIV);
                    if (containsOrIsNotesDIV) {
                        // We are on a TR or a DIV. Go up to the closest TR.
                        const href = $node.closest('tr').find('td.commit a').prop('href');
                        //log('  href ' + href);
                        if (href) {
                            // Extract the commit-ID from the A-tag. href is expected as:
                            // href="/bitbucket/projects/IT/repos/notestestrepo/pull-requests/1/commits/9e39ab2592073baddaabcb035932bed09ae19c41"
                            return href.replace(/.+\//, '');
                        }
                    }
                }
            }
            return undefined;
        }

        /*
        * The Notes-column has a width much more than needed. Reduce it.
        * Setting the width is only effective after the Notes-column has been rendered.
        *
        * It turned out doing this synchronously doesn't work in all cases.
        * This is what I found out so far:
        *   - PR view page, subsequent commit-page after scroll: Needed asynchronous update of width.
        *   - PR create page, first commit-page: Synchonouly update sufficient.
        *   - PR create page, subsequent commit-page after scroll: Needed asynchronous update of width.
        */
        function setNotesColumnWidth() {
            setTimeout(function () {
                // 65px provides space for 3 digits.
                const thNotes = $('table.commits-table th:contains("Notes")');
                thNotes.css({'width': '40px'});
                log(`setNotesColumnWidth(), thNotes ${thNotes.length}`);
            }, 1);
        }

        const CSENotesButtonsObserver = new MutationObserver(function (mutations) {
            mutations.forEach(function (mutation) {
                const commitIdOrTH = getCommitIdIfNotesTD(mutation);
                if (commitIdOrTH) {
                    log('CSENotesButtonsObserver: mutation type: ' + mutation.type
                        + ', target ' + mutation.target
                        + ', addedNodes ' + dbgNodeListToString(mutation.addedNodes)
                        + ', commitIdOrTH ' + commitIdOrTH);

                    if (commitIdOrTH === 'TH') {
                        // Found Notes-TH.
                        setNotesColumnWidth();
                    } else {
                        // commitIdOrTH really contains a commit-ID.
                        setNotesDialogOnPullRequestPage(bitbucketTagdetailsCache.getNotesData(commitIdOrTH));
                    }
                }
            });
        });

        // Observe only on commits-table. On single commit page .observe() would result in error:
        // "Failed to execute 'observe' on 'MutationObserver': parameter 1 is not of type 'Node'."
        // This CSENotesButtonsObserver reacts multiple times on same value for data-last-updated. Don't know why.
        // attribute: data-last-updated
        const config = {
            childList: true,
            //attributes: true,
            // "Set to true to record the previous value of any attribute that changes when monitoring the node or nodes for
            // attribute changes." Only for logging.
            //attributeOldValue: true,
            subtree: true
        };
        CSENotesButtonsObserver.observe(document.body, config);
    }

    registerCSENotesButtonsObserver();

    log('NEW PAGE');

    /*
     * Here are lists of URLs occurring during clicking around. The URLs of interest are marked with "=>"
     *
     * On Commits Page
     *
     *      /bitbucket/projects/PROJECT_1/repos/integrationtest/commits?merges=include&start=0&limit=50&contents
     *      /bitbucket/projects/PROJECT_1/repos/integrationtest/commits?until=refs%2Fheads%2Fb-anno-3&merges=include&start=0&limit=50&contents
     *      /bitbucket/rest/analytics/1.0/publish/bulk
     *      /bitbucket/rest/api/latest/inbox/pull-requests/count
     *      /bitbucket/rest/api/latest/profile/recent/repos?avatarSize=32
     *      /bitbucket/rest/build-status/latest/commits/stats
     *      /bitbucket/rest/jira-integration/latest/servers
     *      /bitbucket/rest/shortcuts/latest/shortcuts/5467926/bundled
     *      /bitbucket/rest/sync/latest/projects/PROJECT_1/repos/integrationtest?at=refs%2Fheads%2Fb-anno-3
     *      /bitbucket/rest/sync/latest/projects/PROJECT_1/repos/integrationtest?at=refs%2Fheads%2Fmaster
     *      /bitbucket/rest/tagdetails/1.0/projects/PROJECT_1/repos/integrationtest/tags?name=a%2Fb&name=a...
     *      /bitbucket/rest/tagdetails/1.0/projects/PROJECT_1/repos/integrationtest/tags?name=anno-3&_=1492025721729
     * =>   /bitbucket/rest/tags/latest/projects/PROJECT_1/repos/integrationtest/tags
     *
     *
     * On Commit View Page
     *  Load
     *
     *      /bitbucket/rest/analytics/1.0/publish/bulk
     *      /bitbucket/rest/api/latest/inbox/pull-requests/count
     *      /bitbucket/rest/api/latest/profile/recent/repos?avatarSize=32
     *      /bitbucket/rest/api/latest/projects/PROJECT_1/repos/integrationtest/commits/f8f321f628d7f9306a735c7b521cd0feb1900e4b/changes?since=f366abab67153550f1122336c8a609b9d84478f5&start=0&limit=1000
     *      /bitbucket/rest/branch-utils/latest/projects/PROJECT_1/repos/integrationtest/branches/info/f8f321f628d7f9306a735c7b521cd0feb1900e4b?limit=10
     *      /bitbucket/rest/build-status/latest/commits/stats/f8f321f628d7f9306a735c7b521cd0feb1900e4b?includeUnique=true
     *      /bitbucket/rest/shortcuts/latest/shortcuts/5467926/bundled
     *      /bitbucket/rest/tagdetails/1.0/projects/PROJECT_1/repos/integrationtest/tags?name=a...
     * =>   /bitbucket/rest/tags/latest/projects/PROJECT_1/repos/integrationtest/tags
     * =>   /bitbucket/projects/IT/repos/integrationtest/commits/e1c1fb09cb7e13991539940df168ae3c64f05b80   since 4.7
     *
     *  Create tag
     *
     *      /bitbucket/rest/analytics/1.0/publish/bulk
     *      /bitbucket/rest/api/latest/projects/PROJECT_1/repos/integrationtest/tags/t
     *      /bitbucket/rest/api/latest/projects/PROJECT_1/repos/integrationtest/tags/t20
     * =>   /bitbucket/rest/git/latest/projects/PROJECT_1/repos/integrationtest/tags
     *      /bitbucket/rest/tagdetails/1.0/projects/PROJECT_1/repos/integrationtest/tags?name=...
     *
     *  Delete tag
     *
     * =>    /bitbucket/rest/git/latest/projects/PROJECT_1/repos/integrationtest/tags/abc
     *
     *
     * On Pull Request Page:
     *  Overview Tab
     *
     *      /bitbucket/rest/jira-integration/latest/servers
     *      /bitbucket/rest/build-status/latest/commits/stats/eb16ff5a93d91801df88e1d73b1299f34bb1f780?includeUnique=true
     *      /bitbucket/rest/api/latest/projects/PROJECT_1/repos/integrationtest/pull-requests/2/activities?start=0&limit=25&avatarSize=64&markup=true
     *      /bitbucket/rest/api/latest/inbox/pull-requests/count
     *      /bitbucket/rest/api/latest/profile/recent/repos?avatarSize=32
     *      /bitbucket/rest/analytics/1.0/publish/bulk
     *      /bitbucket/rest/api/latest/projects/PROJECT_1/repos/integrationtest/pull-requests/2/merge
     *      /bitbucket/rest/api/latest/projects/PROJECT_1/repos/integrationtest/settings/pull-requests
     *      /bitbucket/rest/jira/latest/projects/PROJECT_1/repos/integrationtest/pull-requests/2/issues
     *
     *  Diff Tab
     *
     *      /bitbucket/rest/analytics/1.0/publish/bulk
     *      /bitbucket/rest/api/latest/projects/PROJECT_1/repos/rep_1/pull-requests/1/changes?start=0&limit=1000&changeScope=unreviewed
     *      /bitbucket/rest/api/latest/projects/PROJECT_1/repos/rep_1/pull-requests/1/diff/add_file/add_file.txt?avatarSize=64&markup=true&whitespace&contextLines=-1&withComments=true&autoSrcPath=undefined
     *      /bitbucket/rest/chaperone/1/chaperone/side-by-side-diff-discovery
     *      /bitbucket/rest/webResources/1.0/resources
     *
     *
     *  Commits Tab
     *
     * =>   /bitbucket/projects/PROJECT_1/repos/rep_1/pull-requests/1/commits?start=0&limit=50&contents
     *      /bitbucket/rest/analytics/1.0/publish/bulk
     *      /bitbucket/rest/api/latest/projects/PROJECT_1/repos/rep_1/pull-requests/1/changes?start=0&limit=1000&pullRequestId=1
     *      /bitbucket/rest/api/latest/projects/PROJECT_1/repos/rep_1/pull-requests/1/diff/add_file/add_file.txt?avatarSize=64&markup=true&whitespace&contextLines=-1&withComments=true&autoSrcPath=undefined
     *      /bitbucket/rest/build-status/latest/commits/stats
     *      /bitbucket/rest/tagdetails/1.0/projects/PROJECT_1/repos/rep_1/commits
     *
     */
    $(document).ajaxComplete(function (event, xhr, settings) {

        log('ajaxComplete url ' + settings.url);

        // Exit here if none of the regexes above match the current url
        if (!_.some(URL_REGEX_ARRAY, function (regex) {
            return settings.url.match(regex);
        })) {
            log('  skipping url');
            return;
        }

        log('  processing url');

        // Wait for rendering the response into the DOM
        setTimeout(function () {
                log('  after timeout');

                log('projectKey ' + state.getProject().key + ', repoSlug ' + state.getRepository().slug);

                // TODO: The mutationObserver was to enable/disable to Notes-column in Tag Details versions before
                // Client Side Extensions CSE, where the Notes-Column's visibility could be controlled.
                // But since CSE, the CSE-column is always visible. The TH-element has no class, and the TD-elements
                // are empty when CSE's hidden-attribute is false (also no class).
                // So the mutationObserver is not useful anymore.
                //registerCommitsTableMutationObserver();

                // In case of pull request page, loadTagObjectsByCommitIdsJson() loads like Bitbucket does with
                // '/rest/tags/latest/projects/...'.
                // This results in a response the COMMITS_PAGE_RE reacts on.
                // Because we are on the pull request page in that case, not both code paths (commits page, single commit page) must be
                // executed!
                // We have to check for both cases separate if they are true or false (but they both will never be true at the same time).
                if (settings.url.match(COMMITS_PAGE_RE)) {
                    const isCommitsPage = $('#commits-table').size() > 0;
                    if (isCommitsPage) {
                        log('On page: Commits List');
                        // Tags
                        readTagNamesFromCommitsPageResponse(xhr)
                            .then(loadTagDetailsByTagNames)
                            .then(setTagDetailsDialogsInCommitsList);
                        // Notes
                        readCommitIdsFromCommitsPageResponse(xhr, settings)
                            .then(loadNotesByCommitIdsJson)
                            .then(insertNotesContainersOnCommitsPage)
                            .then(showNotesColumnOnCommitsPage)
                            .then(setNotesDialogsOnCommitsPage);
                    }
                } else if (settings.url.match(COMMIT_VIEW_PAGE_RE)) {
                    log('On page: single Commit');
                    // Tags
                    readTagNamesFromCommitViewPageHtml()
                        .then(loadTagDetailsByTagNames)
                        .then(setTagDetailsDialogsOnCommitViewPage);
                    // Notes
                    // Get the commit-ID:
                    //  - Get it from the URL. In BB < 8 this was achieved by state.getCommit().id. But starting
                    //      with BB 8 the state-object is empty on the commit-view-page.
                    //      An URL could be e. g.:
                    //          - http://localhost:7990/bitbucket/projects/IT/repos/tagstestrepo/commits/48ea6e682a80c46b6b74003e141f1543fe9dbf02#file_1.txt
                    //          - http://localhost:7990/bitbucket/projects/IT/repos/tagstestrepo/commits/48ea6e682a80c46b6b74003e141f1543fe9dbf02
                    //  - Strip off all leading characters until the last slash.
                    //  - Strip off all characters after the hash. The hash consists of 0-9 and a-z. But to be save
                    //      we take also into account the upper A-Z.
                    const commitId = $(location).attr("href").replace(/.+\//, '').replace(/[^0-9a-zA-Z].*/, '');
                    loadNotesByCommitIdsJson('["' + commitId + '"]')
                        .then(setNotesLabelOnCommitViewPage)
                        .then(setNotesDialogOnCommitViewPage);
                } else if (settings.url.match(CREATE_TAG_ON_SINGLE_COMMIT_PAGE_RE) || settings.url.match(DELETE_TAG_ON_SINGLE_COMMIT_PAGE_RE)) {
                    log('On page: Single Commit; Create Tag or Delete Tag');
                    // After the user adds or deletes a tag, the browser re-renders the tag-container. We have to wait until finished.
                    // While adding a tag seems to be fast, deleting a tag is unexpectedly slow. In my local tests, Chrome took ~180ms.
                    // TODO Could be better handled by an Observer?
                    setTimeout(function () {
                        readTagNamesFromCommitViewPageHtml()
                            .then(loadTagDetailsByTagNames)
                            .then(setTagDetailsDialogsOnCommitViewPage);
                    }, 300);
                    // No Notes handling at these events.
                } else if (settings.url.match(PULLREQUEST_VIEW_PAGE_COMMITS_TAB_RE)) {
                    log('On page: Pull-Request view, tab Commits');

                    //
                    //  This page requests all commits as json. In contrast to the Create Pull Request page.
                    //

                    // Tags
                    readCommitIdsFromPullRequestPageResponse(xhr, settings)
                        .then(loadTagObjectsByCommitIdsJson)
                        .then(insertTagsContainersIntoPullRequestCommitsList)
                        .then(loadTagDetailsByTagNames)
                        .then(setTagDetailsDialogsInCommitsList);
                    // Notes
                    readCommitIdsFromPullRequestPageResponse(xhr, settings)
                        .then(loadNotesByCommitIdsJson)
                        .then(addNotesDataToQueue)
                        .then(showCSENotesButtonsWithNotesDataIfButtonsAPIPresent)
                    //.then(insertNotesContainersOnCommitsPage)
                    //.then(showNotesColumnOnCommitsPage)
                    //.then(setNotesDialogsOnCommitsPage);
                } else if (settings.url.match(PULLREQUEST_CREATE_PAGE_COMMITS_TAB_RE) && $('.commits-table').length > 0) {
                    log('On page: Pull-Request create');

                    // Tags
                    readCommitIdsFromPullRequestPageResponse(xhr, settings)
                        .then(loadTagObjectsByCommitIdsJson)
                        .then(insertTagsContainersIntoPullRequestCommitsList)
                        .then(loadTagDetailsByTagNames)
                        .then(setTagDetailsDialogsInCommitsList);
                    // Notes
                    readCommitIdsFromPullRequestPageResponse(xhr, settings)
                        .then(loadNotesByCommitIdsJson)
                        .then(addNotesDataToQueue)
                        .then(showCSENotesButtonsWithNotesDataIfButtonsAPIPresent)
                    //.then(insertNotesContainersOnCommitsPage)
                    //.then(showNotesColumnOnCommitsPage)
                    //.then(setNotesDialogsOnCommitsPage);
                }
            },
            // Wait:
            // General:
            // At this time, the response returned, the browser rendered the tags and registered the tooltips.
            // On commits page:
            // Tag details can be requested in parallel. I assume rendering and registering of tags will be finished
            // until tag details return.
            // On single commit page:
            // Show page / create tag / delete tag: Tag names have to be read from HTML. Wait for finish rendering.
            // At the end of the day, in most use cases calling the above functions have to wait. For convenience, put
            // all in the same setTimeout.
            100
        );
    });

    /**
     * Common functions for tag details and notes.
     */
    function createDialog(triggerElem, dialogId, dialogHtml) {
        log('createDialog(), dialogId ' + dialogId);
        //log('  dialogHtml ' + dialogHtml);

        // Don't care if the triggerElem is already an jQuery-Object or not.
        triggerElem = $(triggerElem);

        // The trigger and the dialog may be appended at different times and in any order.
        // But the trigger has to be complete. This means, the attributes 'data-aui-trigger' and 'aria-controls="..."'
        // have to be part of that trigger element at the time of appending to the DOM.
        // The first attempt of implementation (only to add that attributes) didn't work.
        //
        // "Inline Dialog" https://aui.atlassian.com/aui/7.9/docs/inline-dialog.html:
        //
        // "Associate a trigger (data-aui-trigger) to an inline dialog by setting the trigger element's aria-controls
        // attribute to the id of the inline dialog".
        //
        // The property 'data-aui-trigger' has no value and is set with an empty string ''.
        triggerElem.attr('data-aui-trigger', '');
        triggerElem.attr('aria-controls', dialogId);

        // After adding 'data-aui-trigger' the click on that element doesn't follow the link anymore. To re-create
        // this behaviour, an on-click event-listener has to be added.
        // I have 2 requirements regarding following the link:
        //      1. Open that URL.
        //      2. Allow the user to open the URL in a new tab using usual key+click combinations.
        //
        // The simplest but not functional way would be to add the attribute
        //      target="_blank"
        // But this is not working.
        //
        // The next step was to listen to the click-event and open the URL in the event-listener:
        //      window.location = $(this).attr('href');
        // This fullfilled requiremtn 1), bit not 2). Despite using a usual key/click combination, the link was opened
        // in the same tab.
        //
        // The next try was to add the target="_blank" (when setting "data-aui-trigger" and "aria-controls") in addition
        // to the event-listener. But with no success regarding req. 2).
        //
        // The only way I found was to force opening always a new tab with:
        //      window.open($(this).attr('href'));
        //
        // UN-COOMENT THIS FOR OPENING IN NEW TAB. But the original-Click-event has to be un-registered!
        //triggerElem.find('a').click(function () {
        //    window.open($(this).attr('href'));
        //});

        $('body').append('<aui-inline-dialog id="' + dialogId
            + '" responds-to="hover" alignment="left middle" class="tagdetails-aui-inline-dialog">'
            + dialogHtml
            + '</aui-inline-dialog>');

        // The HIDE-DELAY issue:
        //
        // The dialog has a natural hide-delay of 1000ms and leads to a visibility of about 2000ms. This is too long. Unfortunately,
        // the <aui-inline-dialog> doesn't have the hideDelay parameter as the predecessor had. So we have to hide it manually at
        // mouseleave.
        const $dialog = $('#' + dialogId);
        registerHideDialog(triggerElem, $dialog);
    }

    function registerHideDialog($triggerElem, $dialog) {

        $($triggerElem).mouseleave(function () {
            setTimeout(function () {
                const $hoverdElem = $(':hover');

                //
                //  Check for both trigger-element and dialog to avoid flickering.
                //  BB could fire a weird event-sequence.
                //
                if ($hoverdElem.filter($triggerElem).length === 0 && $hoverdElem.filter($dialog).length === 0) {
                    $($dialog).attr('open', false);
                }
            }, DIALOG_HIDE_DELAY_MS);
        });

        $($dialog).mouseleave(function () {
            setTimeout(function () {
                const $hoverdElem = $(':hover');

                //
                //  Check for both trigger-element and dialog to avoid flickering.
                //  BB could fire a weird event-sequence.
                //
                if ($hoverdElem.filter($triggerElem).length === 0 && $hoverdElem.filter($dialog).length === 0) {
                    $($dialog).attr('open', false);
                }
            }, DIALOG_HIDE_DELAY_MS);
        });
    }

    /**
     * Functions for tag details
     */

    function loadTagDetailsByTagNames(tagNames) {
        log('loadTagDetailsByTagNames()');
        log('  tagNames ' + tagNames);
        let url = buildTagdetailsRestBaseUrl() + '/tags?';
        const encodedTagNameUrlParams = [];
        $.each(tagNames, function (index, value) {
            encodedTagNameUrlParams.push('name=' + encodeURIComponent(value));
        });
        url += encodedTagNameUrlParams.join('&');
        log('  post url ' + url);
        return $.ajax({
            url: url,
            dataType: 'json',
            cache: false
        });
    }

    function loadTagObjectsByCommitIdsJson(commitIdsJsonStr) {
        log('loadTagObjectsByCommitIdsJson()');
        //log('  commitIdsJsonStr ' + commitIdsJsonStr);
        // This is how Bitbucket does it on the commits page.
        // E.g. http://localhost:7990/bitbucket/projects/PROJECT_1/repos/integrationtest/pull-requests/1/commits?start=0&limit=50&contents
        const url = AJS.contextPath() + '/rest/tags/latest/projects/' + state.getProject().key + '/repos/' + state.getRepository().slug + '/tags';
        const deferred = $.Deferred();
        log('  post url ' + url);
        // Omitting contentType results in 'XSRF check failed'!
        $.ajax({
            type: 'POST',
            url: url,
            contentType: "application/json; charset=utf-8",
            data: commitIdsJsonStr,
            dataType: 'json',
            cache: false
        }).done(function (data, textStatus, jqXHR) {
            data.values.length > 0 ? deferred.resolve(data) : deferred.reject();
        }).fail(deferred.reject);
        return deferred;
    }

    // Parameter form ajaxComplete()
    function readTagNamesFromCommitsPageResponse(xhr) {
        log('readTagNamesFromCommitsPageResponse()');
        //log(' xhr ' + JSON.stringify(xhr.responseJSON));
        const tagNames = _.map(xhr.responseJSON.values, function (listElem) {
            return listElem.displayId;
        });
        log('  tagNames ' + tagNames);
        const deferred = $.Deferred();
        tagNames.length > 0 ? deferred.resolve(tagNames) : deferred.reject();
        return deferred;
    }

    function readTagNamesFromCommitViewPageHtml() {
        log('readTagNamesFromCommitViewPageHtml()');
        const tagNames = _.map($('.commit-tags-row a'), function (listElem) {
            const tagName = $(listElem).text();
            return tagName;
        });
        log('  tagNames ' + JSON.stringify(tagNames));
        const deferred = $.Deferred();
        tagNames.length > 0 ? deferred.resolve(tagNames) : deferred.reject();
        return deferred;
    }

    function readCommitIdsFromCommitsPageResponse(xhr, settings) {
        log('readCommitIdsFromCommitsPageResponse()');
        //log('  settings ' + JSON.stringify(settings));
        //log('  xhr ' + JSON.stringify(xhr));
        //log('  settings.data ' + settings.data);
        // In case of commits page response the commit ids are in 'data' as json array.
        const commitIdsJsonStr = settings.data;
        //log('  commitIdsJsonStr ' +commitIdsJsonStr);
        //log('  commitIdsJsonStr parsed ' +commitIdsJsonStr);
        //log('  commitIdsJsonStr len ' + JSON.parse(commitIdsJsonStr).length);

        const deferred = $.Deferred();
        JSON.parse(commitIdsJsonStr).length > 0 ? deferred.resolve(commitIdsJsonStr) : deferred.reject();
        return deferred;
    }

    function readCommitIdsFromPullRequestPageResponse(xhr, settings) {
        log('readCommitIdsFromPullRequestPageResponse()');

        const deferred = $.Deferred();

        // In early versions of Bitbucket the response could be JSON or HTML. But in the meantime all those responses
        // are of format JSON.
        if (settings.dataType !== 'json') {
            return deferred.reject();
        }

        const commitIds = _.map(_.get(xhr, 'responseJSON.values'), function (listElem) {
            return listElem.id;
        });

        log('  commitIdsJsonStr ' + JSON.stringify(commitIds));

        commitIds.length > 0 ? deferred.resolve(JSON.stringify(commitIds)) : deferred.reject();
        return deferred;
    }

    function insertTagsContainersIntoPullRequestCommitsList(data) {
        log('insertTagsContainersIntoPullRequestCommitsList()');
        const url = AJS.contextPath() + '/projects/' + state.getProject().key + '/repos/' + state.getRepository().slug + '/commits?until=';
        //log(' url ' + url);
        const commitIdToTagObjectsMap = {};
        $.each(data.values, function (index, value) {
            const commitId = value.latestCommit;
            if (!commitIdToTagObjectsMap[commitId]) {
                commitIdToTagObjectsMap[commitId] = [];
            }
            commitIdToTagObjectsMap[commitId].push(value);
        });
        log('  commitIdToTagObjectsMap ' + JSON.stringify(commitIdToTagObjectsMap));
        $.each(commitIdToTagObjectsMap, function (key, value) {
            const commitId = key;
            const $tr = $('tr td.commit a[href*="' + commitId + '"]').closest('tr');
            const tagObjects = value;
            const $tagContainer = $(jh.atlassian.bitbucket.tagdetails.panel({
                url: url + encodeURIComponent(tagObjects[0].displayId),
                tags: tagObjects
            }));
            const $messageBody = $tr.find('.message-body');
            $($tagContainer).insertAfter($messageBody);
            $tr.addClass('commit-row has-tags');
        });
        const tagNames = _.map(data.values, function (listElem) {
            return listElem.displayId;
        });
        //log('  tagNames ' + JSON.stringify(tagNames));
        return tagNames;
    }

    function setTagDetailsDialogsInCommitsList(tagNameToTagInfoMap) {
        log('setTagDetailsDialogsInCommitsList()');
        $('span.tag').each(function (index, element) {
            const $element = $(element);
            const tagNames = $element.attr('data-names');
            // Tag names are splitted by ':', which is a forbidden character for tag names.
            const tagNameList = tagNames.split(':');
            const firstTagName = tagNameList[0];
            // The commits page may be paged: On scroll down, the browser loads the next n commits and tags ("infinite scroll").
            // The browser will only load the new tags, and this script does it the same way: This script then will only load the new tags.
            // But $('span.tag') will iterate over all tags received so far. So not all tags the selector gets are in the
            // map. Skip all tags not in the map, because they are already processed.
            if (tagNameToTagInfoMap[firstTagName] == null) {
                return;
            }
            $element.off('mouseenter');  // unbind original tooltip
            const dialogHtml = buildTagDetailsDialogHtml(tagNameList, tagNameToTagInfoMap);
            gDialogIndex += 1;
            const dialogId = 'tagdetails-tag-dialog-' + gDialogIndex;
            createDialog($element, dialogId, dialogHtml);
        });
    }

    function setTagDetailsDialogsOnCommitViewPage(tagNameToTagInfoMap) {
        log('setTagDetailsDialogsOnCommitViewPage()');
        //log('  tagNameToTagInfoMap ' + JSON.stringify(tagNameToTagInfoMap));

        $('.commit-tags-row a').each(function (index, element) {
            const $element = $(element);
            const tagName = $element.text();
            //log('  tagName ' + tagName);
            // buildTagDetailsDialogHtml() requires a list.
            const dialogHtml = buildTagDetailsDialogHtml([tagName], tagNameToTagInfoMap);
            gDialogIndex += 1;
            const dialogId = 'bitbucket-tagdetails-tag-dialog-' + gDialogIndex;
            createDialog($element, dialogId, dialogHtml);
        });
    }

    function buildTagDetailsDialogHtml(tagNameList, tagInfoJsonMap) {
        log('buildTagDetailsDialogHtml()');
        //log('  tagNameList ' + tagNameList + ', tagInfoJsonMap ' + JSON.stringify(tagInfoJsonMap));
        const tagsHtml = [];
        $.each(tagNameList, function (index, tagName) {
            const tagInfo = tagInfoJsonMap[tagName];
            let html = '<div class="tagdetails-dialog-tag">';
            html += '<div>' + tagName + '</div>';
            if (tagInfo != null && tagInfo.tagDetails.isAnnotatedTag) {
                const tagDetails = tagInfo.tagDetails;
                html += '<div>' + _.escape(tagDetails.message) + '</div>';
                html += '<table>'
                    + '<tr><td>Tagger:</td><td>'
                    + _.escape(tagDetails.tagger)
                    + '</td></tr>'
                    + '<tr><td>Date:</td><td>'
                    + _.escape(tagDetails.date)
                    + '</td></tr>';
                if (tagDetails.signature) {
                    html += '<tr><td>'
                        + 'Signature:'
                        + ' </td><td><pre>' + _.escape(tagDetails.signature) + '</pre></td></tr>';
                }
                html += '</div>';   // End of .tagdetails-dialog-tag
                html += '</table>';
            }
            tagsHtml.push(html);
        });
        let dialogHtml = '<div class="tagdetails-dialog tagdetails-dialog-tags">';
        dialogHtml += tagsHtml.join('<hr/>');
        dialogHtml += '</div>';
        return dialogHtml;
    }

    /*
     *  N O T E S
     */

    function loadNotesByCommitIdsJson(commitIdsJsonStr) {
        log('loadNotesByCommitIdsJson()');
        //log('  commitIdsJsonStr ' + commitIdsJsonStr);
        const url = buildTagdetailsRestBaseUrl() + '/notes';
        log('  url ' + url);
        const deferred = $.Deferred();
        // Omitting contentType results in 'XSRF check failed'!
        $.ajax({
            type: 'POST',
            url: url,
            contentType: "application/json; charset=utf-8",
            data: commitIdsJsonStr,
            dataType: 'json',
            cache: false
        }).done(function (data, textStatus, jqXHR) {
            const notesModels = data;
            notesModels.length > 0 ? deferred.resolve(notesModels) : deferred.reject();
        }).fail(deferred.reject);
        return deferred;
    }

    function addNotesDataToQueue(notesModels) {
        log('addNotesDataToQueue()');
        //log('  notesModels ' + JSON.stringify(notesModels));
        $(notesModels).each(function (index, notesModel) {
            const commitId = notesModel['commitId'];
            log('  adding notesModel for commitId ' + commitId);
            bitbucketTagdetailsCache.addNotesData(commitId, notesModel);
        });
        return notesModels;
    }

    function showCSENotesButtonsWithNotesDataIfButtonsAPIPresent(notesModels) {
        log('showCSENotesButtonsWithNotesDataIfButtonsAPIPresent()');
        //log('  notesModels ' + JSON.stringify(notesModels));

        // Check if there are notes-buttons waiting to receive notes-data.
        // If yes, direct the data to a matching notes-button.
        $(notesModels).each(function (index, notesModel) {
            const commitId = notesModel['commitId'];
            const waitingPluginApi = bitbucketTagdetailsCache.getButtonApi(commitId);
            log('  commitId ' + commitId + ', waitingPluginApi ' + waitingPluginApi);
            if (waitingPluginApi) {
                // There is a notes-button waiting to be set-up and enabled.
                // The label is required as string.
                const notesCount = Object.keys(notesModel['namespaceToNotesMap']).length + '';
                log('  notesCount ' + notesCount + ', showing CSE');
                waitingPluginApi.updateAttributes({
                    hidden: false,
                    label: notesCount
                });
            }
        });

        return notesModels;
    }

    function insertNotesContainersOnCommitsPage(notesModels) {
        log('insertNotesContainersOnCommitsPage()');
        //log('  notesModels ' + JSON.stringify(notesModels));
        $.each(notesModels, function (index, element) {
            const notesModel = element;
            const notesCount = _.size(notesModel.namespaceToNotesMap);
            // This container is copied from the comments container. We use the same classes to benefit from the already defined styles.
            const notesContainerHtml = '<span class="comment-count bitbucket-tagdetails-nodes-count">'
                + '<span class="aui-icon aui-icon-small aui-iconfont-page"></span>'
                + '<span >' + notesCount + '</span>' // The comments container here uses the class="count". But this class is display:none on pull request page.
                + '</span>';
            const commentCountSelector = 'tr.commit-row[data-commitid=' + notesModel.commitId + '] td.bitbucket-tagdetails-notesColumn';
            $(commentCountSelector).append(notesContainerHtml);
        });
        return notesModels;
    }

    // TODO Always show the Notes column as it can't be deactivated in CSE.
    function showNotesColumnOnCommitsPage(notesModels) {
        log('showNotesColumnOnCommitsPage()');
        if (notesModels.length > 0) {
            $('.bitbucket-tagdetails-notesColumn').show();
            log('  gHaveNotesSeen = true');
            gHaveNotesSeen = true;
        }
        return notesModels;
    }

    function setNotesDialogsOnCommitsPage(notesModels) {
        log('setNotesDialogsOnCommitsPage()');
        //log('  notesModels ' + JSON.stringify(notesModels));
        $.each(notesModels, function (index, notesModel) {
            const notesCountSelector = 'tr.commit-row[data-commitid=' + notesModel.commitId + '] .bitbucket-tagdetails-nodes-count';
            gDialogIndex += 1;
            const dialogId = 'tagdetails-notes-dialog-' + gDialogIndex;
            const dialogHtml = buildNotesDialogHtml(notesModel.namespaceToNotesMap);
            createDialog(notesCountSelector, dialogId, dialogHtml);
        });
    }

    function setNotesDialogOnPullRequestPage(notesModel) {
        log('setNotesDialogOnPullRequestPage()');
        log('  commitId ' + notesModel.commitId);
        log('  notesModel ' + notesModel);
        const notesNode = $(`tr a[href*=${notesModel.commitId}]`)
            .closest('tr')
            .find('.bitbucket-tagdetails-notesColumn_cse');
        gDialogIndex += 1;
        const dialogId = 'tagdetails-notes-dialog-' + gDialogIndex;
        const dialogHtml = buildNotesDialogHtml(notesModel.namespaceToNotesMap);
        createDialog(notesNode, dialogId, dialogHtml);
    }

    function setNotesLabelOnCommitViewPage(notesModels) {
        log('setNotesLabelOnCommitViewPage()');
        //log('  notesModels ' + JSON.stringify(notesModels));
        // Expect 0 or 1 notes-models.
        if (notesModels.length > 0) {
            const $notesLabel = $(jh.atlassian.bitbucket.tagdetails.notesContentOnCommitView({
                notesCount: _.size(notesModels[0].namespaceToNotesMap)
            }));
            $('.commit-details-summary-panel').append($notesLabel);
        }
        return notesModels;
    }

    function setNotesDialogOnCommitViewPage(notesModels) {
        log('setNotesDialogOnCommitViewPage()');
        if (notesModels.length > 0) {
            const selector = '.bitbucket-tagdetails-notesLabel';
            const dialogId = 'bitbucket-tagdetails-notes-dialog';
            const dialogHtml = buildNotesDialogHtml(notesModels[0].namespaceToNotesMap);
            createDialog(selector, dialogId, dialogHtml);
        }
    }

    function buildNotesDialogHtml(namespaceToNotesMap) {
        log('buildNotesDialogHtml()');
        //log(' namespaceToNotesMap ' + JSON.stringify(namespaceToNotesMap));
        const notes = [];
        $.each(namespaceToNotesMap, function (key, value) {
            const namespace = key;
            const note = value;
            let noteHtml = '<div class="tagdetails-dialog-note">';
            noteHtml += '<div>' + _.escape(namespace) + '</div>';
            noteHtml += '<div><pre>' + _.escape(note) + '</pre></div>';
            noteHtml += '</div>';
            notes.push(noteHtml);
        });
        let dialogHtml = '<div class="tagdetails-dialog">';
        dialogHtml += notes.join('<br/>');
        dialogHtml += '</div>';
        return dialogHtml;
    }


});

$(document).ready(function () {
    require('jh/atlassian/bitbucket/tagdetails');
});
