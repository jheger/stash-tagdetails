const bitbucketTagdetailsCache = (function () {

    return {
        addNotesData(commitId, notesModel) {
            if (commitId) {
                this.notesData[commitId] = notesModel;
            }
        },
        getNotesData(commitId) {
            if (commitId) {
                return this.notesData[commitId];
            }
            return undefined;
        },
        addButtonAPI(commitId, pluginApi) {
            if (commitId) {
                this.buttonApi[commitId] = pluginApi;
            }
        },
        getButtonApi(commitId) {
            if (commitId) {
                return this.buttonApi[commitId];
            }
            return undefined;
        },
        notesData: {},
        buttonApi: {}
    }
})();
